import sys
import cv2
import argparse

from src.tracker import SiamfcTracker
from prednet_tracker import PrednetTracker


paused = True
is_drawing = False
selection_p1 = (0, 0)
selection_p2 = (0, 0)
tracker_started = False
bbox = None


def tracker_select_callback(event, x, y, flags, param):
    global paused, is_drawing, selection_p1, selection_p2, tracker_started, bbox, frame, canvas_frame
    if event == cv2.EVENT_MOUSEMOVE and is_drawing:
        selection_p2 = (x, y)
    if event == cv2.EVENT_LBUTTONDOWN:
        is_drawing = True
        selection_p1 = (x, y)
        selection_p2 = (x, y)
    if event == cv2.EVENT_LBUTTONUP:
        is_drawing = False
        # tracker init & update
        pos_x = (selection_p1[0] + selection_p2[0])/2
        pos_y = (selection_p1[1] + selection_p2[1])/2
        target_width = abs(selection_p1[0] - selection_p2[0])
        target_height = abs(selection_p1[1] - selection_p2[1])

        siamfc_tracker.init_tracking(frame[..., ::-1], pos_x, pos_y, target_width, target_height)
        bbox = siamfc_tracker.update(frame[..., ::-1])
        canvas_frame = frame.copy()
        cv2.rectangle(canvas_frame, (int(bbox[0]), int(bbox[1])), (int(bbox[0]+bbox[2]), int(bbox[1]+bbox[3])), (0, 0, 255), 2, 1)

        prednet_tracker.reset()
        tracker_started = True
        paused = False


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--video_file", help="input video file")
args = parser.parse_args()

WINDOW_NAME = "Tracker Test - Python"
if args.video_file:
    cap = cv2.VideoCapture(args.video_file)
else:
    cap = cv2.VideoCapture(0)

siamfc_tracker = SiamfcTracker()
prednet_tracker = PrednetTracker()

width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback(WINDOW_NAME, tracker_select_callback)

ok, frame = cap.read()
if not ok:
    print 'Cannot read video file'
    sys.exit()
canvas_frame = frame.copy()

cv2.imshow(WINDOW_NAME, frame)

while True:
    keycode = cv2.waitKey(30) & 0xFF
    if keycode == 27:  # escape
        sys.exit()
    if keycode == 32:  # spacebar
        paused = not paused

    if paused:
        canvas_frame = frame.copy()
    else:
        ok, frame = cap.read()
        if not ok:
            print 'Cannot read video file'
            sys.exit()
        canvas_frame = frame.copy()
        if tracker_started:
            bbox = siamfc_tracker.update(frame[..., ::-1])  # BGR -> RGB
            cv2.rectangle(canvas_frame, (int(bbox[0]), int(bbox[1])), (int(bbox[0]+bbox[2]), int(bbox[1]+bbox[3])), (0, 0, 255), 2, 1)

            # PREDNET TRACKING
            prednet_tracker.update(frame[..., ::-1], canvas_frame, bbox)  # BGR -> RGB

    if is_drawing:
        cv2.rectangle(canvas_frame, selection_p1, selection_p2, (255, 255, 255), 2, 1)
    if not paused or is_drawing:
        cv2.imshow(WINDOW_NAME, canvas_frame)





