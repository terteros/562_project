
import cv2


def scale_rectangle(rect, scale):
    cx = rect[0] + rect[2]/2.0
    cy = rect[1] + rect[3]/2.0
    w = rect[2]*scale
    h = rect[3]*scale
    x = cx - w/2.0
    y = cy - h/2.0
    return [x, y, w, h]


def subwindow(img, rect):
    img = cv2.copyMakeBorder(img, rect[3], rect[3], rect[2], rect[2], cv2.BORDER_REPLICATE)
    rect[0] -= rect[2]
    rect[1] -= rect[3]
    return img[int(rect[1]):int(rect[1]+rect[3]), int(rect[0]):int(rect[0]+rect[2])]


def subwindow_around(img, rect, scale):
    rect = scale_rectangle(rect, scale)
    rect = [int(i) for i in rect]
    img = cv2.copyMakeBorder(img, rect[3], rect[3], rect[2], rect[2], cv2.BORDER_REPLICATE)
    rect[0] += rect[2]
    rect[1] += rect[3]
    return img[int(rect[1]):int(rect[1]+rect[3]), int(rect[0]):int(rect[0]+rect[2])]