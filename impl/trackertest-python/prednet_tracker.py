import os
import cv2
import numpy as np

from keras.models import Model, model_from_json
from keras.layers import Input
import tensorflow as tf
from prednet import PredNet

import rect_tools
import matplotlib.pyplot as plt


visualize = True
if visualize:
    plt.ion()
    grid = plt.GridSpec(3, 2)

    fig = plt.figure(figsize=(8, 15))
    ax_preds = plt.subplot(grid[0, :])
    ax_orig = plt.subplot(grid[1, :1])
    ax_corr = plt.subplot(grid[2, :])
    ax_template = plt.subplot(grid[1, 1:])


init = tf.initialize_all_variables()
session = tf.Session()
session.run(init)
tf_img = tf.placeholder('float32', [1, None, None, 3])
tf_kernel = tf.placeholder('float32', [None, None, 3, 1])
conv_op = tf.nn.conv2d(tf_img, tf_kernel, strides=[1, 1, 1, 1], padding='SAME')


def max_response2(search_patch, template):
    template = np.float32(template)
    template -= template.mean()
    search_patch = np.float32(search_patch)
    search_patch -= search_patch.mean()

    search_patch = search_patch[np.newaxis, ...]
    template = template[..., np.newaxis]
    [conv_res] = session.run([conv_op], feed_dict={tf_img: search_patch, tf_kernel: template})
    conv_res = conv_res[0, :, :, 0]
    y, x = np.unravel_index(np.argmax(conv_res), conv_res.shape)

    x = x * 1. / search_patch.shape[2]
    y = y * 1. / search_patch.shape[1]

    return y, x, conv_res


class PrednetTracker(object):
    def __init__(self):
        # Load trained model
        weights_dir = '/media/batuhan/DATA/Doktora/562/Project/prednet/model_data_keras2'
        weights_file = os.path.join(weights_dir, 'prednet_kitti_weights.hdf5')
        json_file = os.path.join(weights_dir, 'prednet_kitti_model.json')
        f = open(json_file, 'r')
        json_string = f.read()
        f.close()
        train_model = model_from_json(json_string, custom_objects={'PredNet': PredNet})
        train_model.load_weights(weights_file)

        # Create testing model (to output predictions)
        layer_config = train_model.layers[1].get_config()
        layer_config['output_mode'] = 'prediction'
        data_format = layer_config['data_format'] if 'data_format' in layer_config else layer_config['dim_ordering']
        test_prednet = PredNet(weights=train_model.layers[1].get_weights(), **layer_config)
        self.input_shape = list(train_model.layers[0].batch_input_shape[1:])
        self.input_shape[0] = 5
        inputs = Input(shape=tuple(self.input_shape))
        predictions = test_prednet(inputs)
        self.test_model = Model(inputs=inputs, outputs=predictions)

        self.prednet_queue = []
        self.pred_result = None
        self.prednet_roi = None
        self.prednet_train_scale = 1
        self.prednet_detect_scale = 4

    def init(self, frames):
        if not len(frames) == self.input_shape[0]:
            pass
        else:
            self.prednet_queue = frames
            # TODO: write this

    def update(self, frame, canvas_frame, bbox):
        if self.pred_result is None:
            self.prednet_roi = bbox
        else:
            detect_patch = rect_tools.subwindow_around(frame, self.prednet_roi, self.prednet_detect_scale)
            ratio = self.prednet_detect_scale / self.prednet_train_scale  # CAUTION : Rounding
            detect_patch = cv2.resize(detect_patch, (self.input_shape[3] * ratio, self.input_shape[2] * ratio),
                                      interpolation=cv2.INTER_AREA)
            # detect_patch = detect_patch.astype(np.float32) / 255
            y, x, corr = max_response2(detect_patch, self.pred_result)
            detect_roi = rect_tools.scale_rectangle(self.prednet_roi, self.prednet_detect_scale)

            if visualize:
                ax_orig.clear()
                ax_orig.imshow(detect_patch)
                ax_orig.set_title('Original')
                ax_orig.set_axis_off()
                ax_orig.plot(x * detect_patch.shape[1], y * detect_patch.shape[0], 'ro')
                ax_corr.imshow(corr, cmap='gray')
                ax_corr.set_title('Cross-correlation')
                ax_corr.set_axis_off()

            cx = detect_roi[0] + x * detect_roi[2]
            cy = detect_roi[1] + y * detect_roi[3]
            self.prednet_roi[0:2] = cx - self.prednet_roi[2] / 2, cy - self.prednet_roi[3] / 2
            cv2.rectangle(canvas_frame, (int(self.prednet_roi[0]), int(self.prednet_roi[1])), (int(self.prednet_roi[0] + self.prednet_roi[2]), int(self.prednet_roi[1] + self.prednet_roi[3])), (0, 255, 255), 2, 1)

        cropped = rect_tools.subwindow_around(frame, self.prednet_roi, self.prednet_train_scale)
        cropped = cv2.resize(cropped, (self.input_shape[3], self.input_shape[2]),
                             interpolation=cv2.INTER_AREA)
        self.prednet_queue.append(cropped)
        if len(self.prednet_queue) > self.input_shape[0]:
            self.prednet_queue = self.prednet_queue[1:]

        if len(self.prednet_queue) == self.input_shape[0]:
            np_array = np.array(self.prednet_queue)

            np_array = np.transpose(np_array, (0, 3, 1, 2))
            np_array = np_array.astype(np.float32) / 255
            pred_results = self.test_model.predict(np_array[np.newaxis, :], 1)
            self.pred_result = np.transpose(pred_results[0, self.input_shape[0]-1], (1, 2, 0))
            self.pred_result = np.uint8(self.pred_result * 255)

            if visualize:
                pred_input = np.hstack(tuple(self.prednet_queue))
                pred_output = np.transpose(pred_results[0], (0, 2, 3, 1))
                pred_output = [pred_output[i] for i in range(0, pred_output.shape[0])]
                pred_output = np.hstack(tuple(pred_output))
                pred_output = np.uint8(pred_output * 255)
                result_image = np.vstack((pred_input, pred_output))
                ax_preds.imshow(result_image)
                ax_preds.set_title('First Row: Input, Second Row: Output')
                ax_preds.set_axis_off()
                ax_template.imshow(self.pred_result)
                ax_template.set_axis_off()
                ax_template.set_title('Prediction')
                fig.canvas.draw()

        return self.prednet_roi

    def reset(self):
        self.prednet_queue = []
        self.pred_result = None
        self.prednet_roi = None



