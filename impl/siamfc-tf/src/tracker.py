
import tensorflow as tf
print('Using Tensorflow '+tf.__version__)
import matplotlib.pyplot as plt
import sys
# sys.path.append('../')
import os
import csv
import numpy as np
from PIL import Image
import time

import src.siamese as siam
from src.visualization import show_frame, show_crops, show_scores
from src.parse_arguments import parse_arguments

import cv2


class SiamfcTracker(object):

    def __init__(self):
        hp, evaluation, run, env, design = parse_arguments(
            param_path='/media/batuhan/Local/UBUNTU/562/562_project/impl/siamfc-tf/parameters')
        final_score_sz = hp.response_up * (design.score_sz - 1) + 1
        # build TF graph once for all
        filename, image_file_reader, image, templates_z, scores = siam.build_tracking_graph(final_score_sz, design, env)

        self.hp = hp
        self.run = run
        self.scale_factors = hp.scale_step ** np.linspace(-np.ceil(hp.scale_num / 2), np.ceil(hp.scale_num / 2), hp.scale_num)
        self.final_score_sz = final_score_sz
        self.hann_1d = np.expand_dims(np.hanning(final_score_sz), axis=0)
        self.penalty = np.transpose(self.hann_1d) * self.hann_1d
        self.penalty = self.penalty / np.sum(self.penalty)
        self.design = design

        self.filename = filename
        self.image_file_reader = image_file_reader
        self.image = image
        self.templates_z = templates_z
        self.scores = scores

        self.pos_x = None
        self.pos_y = None
        self.target_w = None
        self.target_h = None
        self.z_sz = None
        self.x_sz = None
        self.coord = None
        self.threads = None
        self.sess = None
        self.templates_z_ = None

    def init_tracking(self, frame, pos_x, pos_y, target_w, target_h):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.target_w = target_w
        self.target_h = target_h
        context = self.design.context * (target_w + target_h)
        self.z_sz = np.sqrt(np.prod((target_w+context)*(target_h+context)))
        self.x_sz = float(self.design.search_sz) / self.design.exemplar_sz * self.z_sz

        self.sess = tf.Session()
        tf.global_variables_initializer().run(session=self.sess)
        # Coordinate the loading of image files.
        self.coord = tf.train.Coordinator()
        self.threads = tf.train.start_queue_runners(coord=self.coord, sess=self.sess)

        # save first frame position (from ground-truth)
        bbox = pos_x - target_w / 2, pos_y - target_h / 2, target_w, target_h

        _, self.templates_z_ = self.sess.run([self.image, self.templates_z], feed_dict={
            siam.pos_x_ph: pos_x,
            siam.pos_y_ph: pos_y,
            siam.z_sz_ph: self.z_sz,
            self.image_file_reader: frame})
        return bbox

    def read_image_file(self, frame_name):
        image_file_handler = self.sess.run(self.image_file_reader, feed_dict={self.filename: frame_name})
        return image_file_handler

    @staticmethod
    def read_image_file_cv(frame_name):
        im = cv2.imread(frame_name)
        im = im[..., ::-1]
        return im

    def update(self, image_file_handler):
        scaled_exemplar = self.z_sz * self.scale_factors
        scaled_search_area = self.x_sz * self.scale_factors
        scaled_target_w = self.target_w * self.scale_factors
        scaled_target_h = self.target_h * self.scale_factors

        image_, scores_ = self.sess.run(
            [self.image, self.scores],
            feed_dict={
                siam.pos_x_ph: self.pos_x,
                siam.pos_y_ph: self.pos_y,
                siam.x_sz0_ph: scaled_search_area[0],
                siam.x_sz1_ph: scaled_search_area[1],
                siam.x_sz2_ph: scaled_search_area[2],
                self.templates_z: np.squeeze(self.templates_z_),
                self.image_file_reader: image_file_handler
            }, **{})
        # _, image_, scores_ = self.sess.run(
        #     [self.image_file_reader, self.image, self.scores],
        #     feed_dict={
        #         siam.pos_x_ph: self.pos_x,
        #         siam.pos_y_ph: self.pos_y,
        #         siam.x_sz0_ph: scaled_search_area[0],
        #         siam.x_sz1_ph: scaled_search_area[1],
        #         siam.x_sz2_ph: scaled_search_area[2],
        #         self.templates_z: np.squeeze(self.templates_z_),
        #         self.filename: frame_name,
        #     }, **{})
        scores_ = np.squeeze(scores_)
        # penalize change of scale
        scores_[0, :, :] = self.hp.scale_penalty * scores_[0, :, :]
        scores_[2, :, :] = self.hp.scale_penalty * scores_[2, :, :]
        # find scale with highest peak (after penalty)
        new_scale_id = np.argmax(np.amax(scores_, axis=(1, 2)))
        # update scaled sizes
        x_sz = (1 - self.hp.scale_lr) * self.x_sz + self.hp.scale_lr * scaled_search_area[new_scale_id]
        target_w = (1 - self.hp.scale_lr) * self.target_w + self.hp.scale_lr * scaled_target_w[new_scale_id]
        target_h = (1 - self.hp.scale_lr) * self.target_h + self.hp.scale_lr * scaled_target_h[new_scale_id]
        # select response with new_scale_id
        score_ = scores_[new_scale_id, :, :]
        score_ = score_ - np.min(score_)
        score_ = score_ / np.sum(score_)
        # apply displacement penalty
        score_ = (1 - self.hp.window_influence) * score_ + self.hp.window_influence * self.penalty
        pos_x, pos_y = _update_target_position(self.pos_x, self.pos_y, score_, self.final_score_sz, self.design.tot_stride, self.design.search_sz, self.hp.response_up, x_sz)
        # convert <cx,cy,w,h> to <x,y,w,h> and save output
        bbox = [pos_x - target_w / 2, pos_y - target_h / 2, target_w, target_h]
        # update the target representation with a rolling average
        if self.hp.z_lr > 0:
            new_templates_z_ = self.sess.run([self.templates_z], feed_dict={
                siam.pos_x_ph: pos_x,
                siam.pos_y_ph: pos_y,
                siam.z_sz_ph: self.z_sz,
                self.image: image_
            })

            self.templates_z_ = (1 - self.hp.z_lr) * np.asarray(self.templates_z_) + self.hp.z_lr * np.asarray(new_templates_z_)

        # update template patch size
        z_sz = (1 - self.hp.scale_lr) * self.z_sz + self.hp.scale_lr * scaled_exemplar[new_scale_id]

        if self.run.visualization:
            show_frame(image_, bbox, 1)

        return bbox

    def close(self):
        self.coord.request_stop()
        self.coord.join(self.threads)

        # from tensorflow.python.client import timeline
        # trace = timeline.Timeline(step_stats=run_metadata.step_stats)
        # trace_file = open('timeline-search.ctf.json', 'w')
        # trace_file.write(trace.generate_chrome_trace_format())
        plt.close('all')


# read default parameters and override with custom ones
def tracker(hp, run, design, frame_name_list, pos_x, pos_y, target_w, target_h, final_score_sz, filename, image_file_reader, image, templates_z, scores, start_frame):
    siamfc_tracker = SiamfcTracker(hp, run, design, final_score_sz, filename, image_file_reader, image, templates_z, scores)
    image_handler = siamfc_tracker.read_image_file_cv(frame_name_list[0])
    siamfc_tracker.init_tracking(image_handler, pos_x, pos_y, target_w, target_h)

    num_frames = np.size(frame_name_list)
    # stores tracker's output for evaluation
    bboxes = np.zeros((num_frames, 4))

    t_start = time.time()
    for i in range(1, num_frames):
        image_handler = siamfc_tracker.read_image_file(frame_name_list[i])
        bboxes[i, :] = siamfc_tracker.update(image_handler)
    t_elapsed = time.time() - t_start
    speed = num_frames/t_elapsed

    siamfc_tracker.close()
    return bboxes, speed


def _update_target_position(pos_x, pos_y, score, final_score_sz, tot_stride, search_sz, response_up, x_sz):
    # find location of score maximizer
    p = np.asarray(np.unravel_index(np.argmax(score), np.shape(score)))
    # displacement from the center in search area final representation ...
    center = float(final_score_sz - 1) / 2
    disp_in_area = p - center
    # displacement from the center in instance crop
    disp_in_xcrop = disp_in_area * float(tot_stride) / response_up
    # displacement from the center in instance crop (in frame coordinates)
    disp_in_frame = disp_in_xcrop *  x_sz / search_sz
    # *position* within frame in frame coordinates
    pos_y, pos_x = pos_y + disp_in_frame[0], pos_x + disp_in_frame[1]
    return pos_x, pos_y


