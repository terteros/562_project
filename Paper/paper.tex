\documentclass[conference]{IEEEtran}

% Some Computer Society conferences also require the compsoc mode option,
% but others use the standard conference format.
%
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[conference]{../sty/IEEEtran}

% *** Do not adjust lengths that control margins, column widths, etc. ***
% *** Do not use packages that alter fonts (such as pslatex).         ***
% There should be no need to do such things with IEEEtran.cls V1.6 and later.
% (Unless specifically asked to do so by the journal or conference you plan
% to submit to, of course. )

\usepackage{graphicx}
\graphicspath{ {images/} }
% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Visual Object Tracking by Next-Frame Prediction}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{\IEEEauthorblockN{Batuhan Karagoz}
\IEEEauthorblockA{METU}
}

% conference papers do not typically use \thanks and this command
% is locked out in conference mode. If really needed, such as for
% the acknowledgment of grants, issue a \IEEEoverridecommandlockouts
% after \documentclass

% for over three affiliations, or if they all won't fit within the width
% of the page, use this alternative format:
% 
%\author{\IEEEauthorblockN{Michael Shell\IEEEauthorrefmark{1},
%Homer Simpson\IEEEauthorrefmark{2},
%James Kirk\IEEEauthorrefmark{3}, 
%Montgomery Scott\IEEEauthorrefmark{3} and
%Eldon Tyrell\IEEEauthorrefmark{4}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}School of Electrical and Computer Engineering\\
%Georgia Institute of Technology,
%Atlanta, Georgia 30332--0250\\ Email: see http://www.michaelshell.org/contact.html}
%\IEEEauthorblockA{\IEEEauthorrefmark{2}Twentieth Century Fox, Springfield, USA\\
%Email: homer@thesimpsons.com}
%\IEEEauthorblockA{\IEEEauthorrefmark{3}Starfleet Academy, San Francisco, California 96678-2391\\
%Telephone: (800) 555--1212, Fax: (888) 555--1212}
%\IEEEauthorblockA{\IEEEauthorrefmark{4}Tyrell Inc., 123 Replicant Street, Los Angeles, California 90210--4321}}




% use for special paper notices
%\IEEEspecialpapernotice{(Invited Paper)}




% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract
\begin{abstract}
Visual Object Tracking has seen substantial progress in recent years largely due to the introduction of discriminative learning methods operating on high dimensional feature spaces. Despite this progress, most work is confined in between these two ends. Instead we propose a novel tracking model that builds on PredNet model introduced for Video Prediction problem. Using PredNet to generate the target appearance in the future frames, our method employs detection by normalized cross correlation in the RGB domain. While producing interpretable appearance predictions, our tracker performs comparable against state-of-the art methods.     
\end{abstract}

% no keywords




% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}
% no \IEEEPARstart
Visual tracking is one of the most challenging problems
in computer vision with many application areas such as surveillance and autonomous
driving, robotics etc. In this paper, we consider single object tracking where the goal is to estimate the target trajectory in a sequence of images from the target's initial state. In its most generic form, this problem does not assume any restriction on targets. This generic nature makes the problem difficult with several issues arising such as appearance changes, camera motion, illumination changes, deformations, occlusions. 

Up to date, the most successful trackers employed an appearance model for the target either learned online or offline. This appearance model is then used as a template searched over a region to perform detection. Correlation Filter(CF) trackers have been given the most interest between online learning models due to their tracking performance and high frame rates. CF tracking approaches have seen many improvements over the recent years \cite{CCOT},\cite{KCF},\cite{CACF}. On the offline learning side, Deep Learning models have shown the best performance mostly due to their ability to learn from large datasets. Deep Features are also used as feature extractors in the existing CF frameworks \cite{CCOT},\cite{DeepDCF}. Current best performing trackers either use CF approach or deep models or combine the two. 

Almost any state-of-the art tracker perform detection on a high dimensional feature space. This  decreases their detection speed and the interpretability of their appearance model. In theory, it is possible to detect the target perfectly with a perfect appearance model in the RGB space. Following this insight, we aim to learn a well enough appearance model in RGB space that simple cross correlation would produce desirable tracking results.

Predicting the target appearance in the next frame corresponds to video next-frame prediction which is another major research area in computer vision. Despite the generic nature of the problem, it is possible to use abundant unlabeled video data for learning. Recently, several neural network models have been suggested to predict future frames of a video sequence from the past frames\cite{lecun},\cite{vpn},\cite{prednet}. One such model is the PredNet architecture that is inspired by the predictive coding phenomenon observed in the human brain\cite{prednet}. PredNet is a Recurrent Convolutional Neural Network that transfer information with both bottom-up and top-down connections. Bottom-up connections are the core part of the hierarchical representation learning, whereas top-down recurrent connections are borrowed from the predictive-coding concept.  

Building on the PredNet model, we propose the PredNet Tracker for real-time single object tracking. PredNet Tracker uses the PredNet to generate the appearance of the scene around the target for the next frame. We employ detection by cross-correlation with this appearance in the RGB domain. Our tracking model exploits both unlabeled video data and labeled tracking data. We evaluate different loss functions for the offline training phase. The results show that PredNet Tracker model performs comparably against state-of-the art trackers, thereby opens up a new research path for the single target tracking problem.    




\section{Related Work}
\subsection{Deep Learning in Tracking}
After the resurgence of CF tracking methods with KCF Tracker\cite{KCF}, several works have  investigated enhancing CF framework with existing CNN models, mostly object classification and detection models. Despite online learning phase inside CF framework, these trackers suffer from generalization issues arising from their CNN feature extractors. Several trackers such as SO-DLT and MDNet applies online updates to their networks similar to single instance Stochastic Gradient Descent. 
Siamese networks have also received interest as high dimensional embedding functions. GOTURN \cite{goturn} uses two seperate CNNs combined with a fully connected network to regress object bounding box directly. Tao et al.\cite{sint} proposed Siamese Instance Search(SINT) that comprises of a Siamese Network performing as a matching function between patching and a patch proposal strategy. Similary SiamFC uses a Siamese Network where two sides combined with a cross correlation layer. Their model differs from the SINT as they use a pair of cloned Fully Convolutional Network. The advantage is that it is possible to perform detection on an arbitrary size search region. 
Although scarce, Recurrent models are also proposed for tracking. Gan et al. \cite{gan2015first} suggests a GRU\cite{chung2014empirical} based model on top of a CNN feature extractor to obtain a probability distribution of target location over pixels. Kahou et al.\cite{kahou2015ratm} extends RNNs with an attention mechanism to eleviate the search process.
     
\subsection{Video Prediction}
Until recently, work on video prediction has remained limited to synthetic datasets and single step prediction. With Ranzato et al.\cite{ranzato2014video} defining a baseline for Frame prediction, methods have been proposed to tackle with real life video sequences and predictions beyond single step. Srivastava et al.\cite{srivastava2015unsupervised} used LSTM units for future frame prediction. Matthiu et al.\cite{lecun} investigated different loss functions to reduce blur effects in predictions. In Finn et al.\cite{finn2016unsupervised}, a stack of LSTMs is used by three different strategy to directly model the pixel motion.      

\section{Predictive Tracking Model}

We consider the single target short term tracking problem. For a given rectangular target in the initial frame, our aim is to find the corresponding rectangular bounding boxes of the target in the future frames. PredNet Architecture is concisely introduced first (\ref{PredNet}). Subsequently, PredNet Tracker model is formulated in \ref{PredNetT}. Training strategies are discussed in \ref{Training}.  

\subsection{PredNet Architecture}\label{PredNet}
\begin{figure*}
\begin{center}
\includegraphics[width=15cm]{PredNet}
\end{center}
   \caption{PredNet Architecture}
\label{fig:PredNet}
\end{figure*}
The PredNet architecture is a recurrent deep neural network. Each layer generates a prediction of its input for the next time frame. The difference between this prediction and the layer input is fed up to the next layer as input. Precisely speaking, each layer is the composition of four modules: An LSTM unit $R_l$ outputs internal state that is used to generate the layer prediction via a convolutional unit $\hat{A}_l$. This prediction is compared against the layer input followed by a convolutional and pooling layer that together forms $A_l$. Error is calculated by taking the difference of $\hat{A}_l$ and $A_l$ and splitting the negative and positive terms of the difference. Whole model is depicted in Figure [FIGURE REF!]. 

The PredNet operates in two different phases for each incoming frame. First, Recurrent units are updated in a top-down manner in following way.
$$R^t_l = ConvLSTM(E^{t-1}_l, R^{t-1}_l, Upsample(R^t_{l+1})) $$
These state variables are fed into convolutional units followed by ReLU nonlinearities to generate the layer prediction:
$$\hat{A}^t_l = ReLU(Conv(R^t_l)) $$.

This phase in fact corresponds to a recurrent deconvolutional network. The prediction output of the whole model is then $A^t_0$. 
In second phase, Errors are calculated using the actual input. The PredNet model is trained via Backprogation Algorithm with Stochastic Gradient Descent(SGD). The loss function is a weighted sum of the error terms where weights are hyperparameters of the training procedure.     

\subsection{Predictive Tracking Model}\label{PredNetT}
The PredNet Tracker consists of a PredNet Instance and a normalized cross correlation module(see figure 1). Prednet instance is used to generate the prediction of the frame patch around the target. Cross correlation module outputs a spatial score map for the target position. Target is then predicted to be in the peak of this score map in the new frame. 
Let $\Phi$ denote the recurrent Prednet Function, i.e. 
$$\Phi(X_t,R_t) = (\hat{X}_{t+1},R_{t+1}) $$ 
where $X_t$ is the input patch, $R_t$ and $R_{t+1}$ are model states and $\hat{X}_{t+1}$ is the prediction generated. For each new frame, we obtain $X_t$ by cropping the frame by the target bounding box scaled by a scalar $\lambda$ to add some context. 
Let $\mathcal{N}(,)$ be the normalized cross correlation operator. Then, we obtain the target position in the next frame as follows:
$$ p_{t+1} = \arg\max_{x,y}(\hat{s}_{t+1}[x,y])) $$  
where $$\hat{s}_{t+1} = \mathcal{N}(X_{t+1},\hat{X}_{t+1})$$ is the score map.
Scale changes are handled by applying the multiscale search strategy. For each scale step, we sample crops and resize them to the original scale and generate score maps for each scale. Peak over all score maps multiplied by penalty terms gives the target position.    

\subsection{Training Procedure}\label{Training}
The original PredNet model is trained via SGD over unlabeled video sequences. In our work, SGD is also used for training, however we add a loss term for the tracking accuracy used in Bertinetto et al.\cite{siamfc} to the Loss function. 
$$L_{tracking} = \frac{1}{|D|}\sum_{(x,y)\in D}log(1+exp(-s[x,y]\hat{s}[x,y])), $$
$$L_{train} = L_{vanilla} + L_{tracking} $$
Here $s[x,y]$ is the groundtruth for the target position. 
Note that $L_{vanilla}$ is defined directly over PredNet output, whereas $L_{tracking}$ is defined over correlation output. To backpropogate Tracking error to the network, we treat the correlation module $\mathcal{N}$ as a network layer and defined its derivative over the network prediction.

Since $L_{tracking}$ is a supervised loss term, it requires training data labeled with target position sequences. 

\section{Experiments} 
\subsection{Model Variants}

\begin{table*}
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
\hline
Model & Prediction Loss & Tracking Loss & KITTI & OTB \\
\hline\hline
PredNet-V & Yes & No & Raw & No\\
PredNet-KT & Yes & Yes & Raw and Tracking  & No\\
PredNet-KT+OTB & Yes & Yes & Raw and Tracking  & Yes\\
\hline
\end{tabular}
\end{center}
\caption{Model Variants}
\end{table*}

 We evaluate loss functions defined in \ref{Training}, using KITTI Raw Dataset\cite{kitti} for Vanilla Loss and KITTI Tracking\cite{kitti} and OTB\cite{otb} Dataset for Tracking Loss. While showing the results, PredNet-V denotes the variant with the original Prednet, PredNet-KT denotes the variant trained with both Loss terms on KITTI Tracking Dataset and PredNet-KT+OTB denotes the variant trained with both Loss terms on both datasets. Variants are summarized in Table I.
 
\subsection{Results}
We evaluate our tracker on VOT2017 Dataset using VOT Evaluation format\cite{vot}. VOT toolkit restarts a tracker in the case of a failure that is when overlap between the tracker output and the groundtruth falls below a certain threshold. Results are presented  in Table II for accuracy(overlap ratio), robustness(failure rate) and EAO\cite{vot} that is unique to the VOT format.
We compare our tracker with  four state-of-the-art trackers KCF\cite{KCF},SRDCF\cite{srdcf},SiamFC\cite{siamfc}. Results for NCC Tracker defined in VOT Toolkit are also given as a baseline method.

\begin{table*}
\begin{center}
\begin{tabular}{|l|c|c|c|}
\hline
Model & Accuracy & Robustness & EAO \\
\hline\hline
KCF & 0.447 & 0.773 & 0.135 \\
SRDCF & 0.490 & 0.974 & 0.119 \\
SiamFC & 0.502 & 0.585 & 0.188 \\
NCC & 0.290 & 2.102 & 0.050 \\
\hline
PredNet-V & 0.390 & 1.046 & 0.09 \\
PredNet-KT & 0.489 & 0.630 & 0.138 \\
PredNet-KT+OTB & 0.492 & 0.602 & 0.151 \\
\hline
\end{tabular}
\end{center}
\caption{Results}
\end{table*}

\subsection{Discussion}
Our model underperforms SiamFC despite being similar to it in terms of methodology in both tracking and training phases. We believe that this is due to SiamFC being a much simpler model with only 3 convolutional layers whereas PredNet Architecture uses LSTM's known to be hard to train. 

PredNet Tracker largely outperforms baseline NCC Tracker. Note that NCC tracker is very similar to ours with only difference being the appearence model. This supports the idea that as prediction in RGB domain gets better, NCC method improves as well. 

Results show that Supervised Tracking Loss is crucial for the adaption of the PredNet architecture to the tracking task. Introducing OTB dataset also helps probably because variance of KITTI Dataset is very low compared to the test dataset VOT2016.  
\section{Conclusion}
In this work, we proposed PredNet Tracker. It is a Normalized Cross Correlation based tracker that uses an existing Next Frame Prediction model, PredNet. Furthermore, we defined a hybrid training strategy that employs both an unsupervised prediction loss and supervised tracking loss. Experimental evaluation shows that our model is able to improve over the baseline NCC tracker method and it performs promising compared to state-of-the-art trackers. We believe that this  points into a brand new research direction and hope that this avenue better explored in future.     




{\small
\bibliographystyle{ieee}
\bibliography{egbib}
}




% that's all folks
\end{document}


